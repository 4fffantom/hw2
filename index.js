const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();
const PORT =  8080;
const key=process.env.MYKEY;


const { trucksRouter } = require('./src/controllers/trucksController');
const { usersRouter } = require('./src/controllers/usersController');
const { authRouter } = require('./src/controllers/authController');
const { authMiddleware } = require('./src/middlewares/authMiddleware');
const { loadsRouter } = require('./src/controllers/loadsController');

app.use(express.json());


app.use(morgan('tiny'));
app.use('/api/auth', authRouter);
app.use(authMiddleware);
app.use('/api/users/me', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);


app.use((req, res, next) => {
    res.status(404).json({ message: 'Not found' });
});

app.use((err, req, res, next) => {
    res.status(500).json({ message: err.message });
});

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://me:'+key+'@database.jubr9.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
            useNewUrlParser: true, useUnifiedTopology: true
        });
        app.listen(PORT);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
};

start();