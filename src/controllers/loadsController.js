const express = require('express');
const router = new express.Router();


const {
  addLoadForUser,
  getLoadsByUserId,
  getActiveLoadForDriverById,
  getLoadByIdForUser,
  updateLoadByIdForUser,
  updateLoadStateForDriverById,
  findTruckForLoadByLoadId,
} = require('../services/loadsService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');


router.post('/', asyncWrapper(async (req, res) => {
  console.log('f');
  const {userId} = req.user;
  const data = req.body;
  await addLoadForUser(userId, data);
  res.status(200).json({message: 'Load created successfully\''});
}));

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const status = req.query.status;
  const limit = req.query.limit && parseInt(req.query.limit) <= 50 ?
      parseInt(req.query.limit) : 10;
  const offset = req.query.offset ? parseInt(req.query.offset) : 0;

  const loads = await getLoadsByUserId(userId, status, offset, limit);

  res.json({loads});
}));

router.get('/active', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const load = await getActiveLoadForDriverById(userId);
  res.json({load});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  const load = await getLoadByIdForUser(id, userId);
  res.json({load});
}));



// eslint-disable-next-line max-len
router.put('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const data = req.body;

  const updatedLoad = await updateLoadByIdForUser(id, userId, data);

  if (!updatedLoad) {
    throw new Error('Only "NEW" load can be updated');
  }

  res.json({message: 'Load details changed successfully'});
}));




router.patch('/active/state', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  const newState = await updateLoadStateForDriverById(userId);

  res.json({message: `Load state changed to '${newState}'`});
}));


router.post('/:id/post', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const isFound = await findTruckForLoadByLoadId(id, userId);
  const message = isFound ? 'Load posted successfully' : 'Please try again';

  res.json({
    message: message,
    driver_found: isFound,
  });
}));
module.exports = {
  loadsRouter: router,
};
