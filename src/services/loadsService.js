const {Load} = require('../models/loadModel');
const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');

const addLoadForUser = async (userId, data) => {
  console.log('ya ustal');
  const user = await User.findOne({_id: userId});
  if (!user) {
    throw new Error('No such user found');
  }
  if (user.role==='DRIVER') {
    throw new Error('Available only for shippers');
  }
  const load = new Load({...data, created_by: userId, _id: userId});
  await load.save();
};

const getLoadsByUserId = async (userId, status, offset=0, limit=10) => {
  let loads;
  const user = await User.findOne({_id: userId});
  if (!user) {
    throw new Error('Invalid user');
  }
  const role =user.role;

  if (role==='DRIVER') {
    loads = await Load.find({assigned_to: userId}, '-__v')
        .skip(offset)
        .limit(limit);
  }
  if (role==='SHIPPER') {
    loads = await Load.find({created_by: userId}, '-__v')
        .skip(offset)
        .limit(limit);
  }
  return loads;
};

const getActiveLoadForDriverById = async (driverId) => {
  const activeLoad = await Load.findOne({status: 'ASSIGNED',
    assigned_to: driverId}, '-__v');
  return activeLoad || [];
};


const getLoadByIdForUser = async (loadId, created_by) => {
  const load = await Load.findOne({_id: loadId, created_by}, '-__v');
  if (!load) {
    throw new Error('No load with such id found!');
  }
  return load;
};

const updateLoadByIdForUser = async (loadId, created_by, data) => {
  const updatedLoad = await Load.findOneAndUpdate({_id: loadId, created_by,
    status: 'NEW'}, {$set: data});
  return updatedLoad;
};

const updateLoadStateForDriverById = async (userId) => {
  const state = ['En route to Pick Up', 'Arrived to Pick Up',
    'En route to delivery', 'Arrived to delivery'];
  try {
    const activeLoad = await Load.findOne({status: 'ASSIGNED', assigned_to: userId});
    let newState;
    if (activeLoad) {
      const currState = activeLoad.state;
      const newStateIndex = state.indexOf(currState) + 1;
      if (newStateIndex <= 3 && newStateIndex > 0) {
        newState = state[newStateIndex];
        await Load.updateOne({assigned_to: userId}, {$set: {state: newState}});
      }
      if (newState === 'Arrived to delivery') {
        await Load.updateOne({assigned_to: userId}, {$set: {status: 'SHIPPED'}});
        await Truck.findOneAndUpdate({assigned_to: userId}, {$set: {status: 'IS'}});
      }
      return newState;
    } else {
      throw new Error('No active loads found');
    }
  } catch (err) {
    throw new Error(err.message);
  }
};

const findTruckForLoadByLoadId = async (loadId, created_by) => {
  const load = await Load.findOneAndUpdate({_id: loadId, created_by},
      {$set: {status: 'POSTED'}});
  const truck = await Truck.findOne({status: 'IS', assigned_to: {$ne: null}});
  if (truck) {
    const activeTruck = await Truck.findOneAndUpdate({_id: truck._id},
        {$set: {status: 'OL'}});
    await Load.findOneAndUpdate({_id: loadId, created_by}, {$set:
          {status: 'ASSIGNED', state: 'En route to Pick Up',
            assigned_to: activeTruck.assigned_to}});
    return true;
  } else {
    await Load.findOneAndUpdate({_id: loadId, created_by},
        {$set: {status: 'NEW'}});
    return false;
  }
};

module.exports = {
  addLoadForUser,
  getLoadsByUserId,
  getActiveLoadForDriverById,
  getLoadByIdForUser,
  updateLoadByIdForUser,
  updateLoadStateForDriverById,
  findTruckForLoadByLoadId,
};

