const {Truck} = require('../models/truckModel');

const getUsersTrucks = async (userId) => {
  const trucks = await Truck.find({created_by: userId});
  if (!trucks) {
    throw new Error('No truck with such id found');
  }
  return {
    trucks,
  };
};

const addTruckForDriver = async (created_by, data) => {
  const truck = new Truck({...data, created_by});
  await truck.save();
};

const getUsersTruckById = async (truckId, userId) => {
  const truck = await Truck.findOne({_id: truckId, created_by: userId}, '-__v');
  if (!truck) {
    throw new Error('No truck with such id found!');
  }
  return truck;
};

const deleteUsersTruckById = async (truckId, created_by) => {
  const truck = await Truck.findOne({_id: truckId, created_by});
  if (!truck) {
    throw new Error('No truck with such id found!');
  }
  await Truck.findOneAndRemove({_id: truckId, created_by});
};

const assignTruckToUserById = async (truckId, userId) => {

  await Truck.findOneAndUpdate({_id: truckId}, {$set: {assigned_to: userId}} );
};

module.exports = {
  getUsersTrucks,
  addTruckForDriver,
  getUsersTruckById,
  deleteUsersTruckById,
  assignTruckToUserById,
};
